﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlContainer : MonoBehaviour
{
    public string nameAssetBundle;

    public bool modelDownloaded;

    // Start is called before the first frame update
    void OnEnable()
    {
        print("prende");
        if (!modelDownloaded)
        {
            Manager.instance.modelsCharged = false;
            Manager.instance.URLAB = null;
            Manager.instance.FirebaseURL(nameAssetBundle);
            Manager.instance.CreateList(gameObject.name);
            StartCoroutine(EsperaModelo());
        }
        else
            Muestralo();
    }

    IEnumerator EsperaModelo()
    {
        while(!Manager.instance.modelsCharged)
            yield return null;
        Muestralo();
    }

    void Muestralo()
    {
        int indexmodel = 0;
        foreach (Modelos model in Manager.instance.modelos)
        {
            if (model.modelo.name == gameObject.name)
            {
                //Manager.instance.MuestraModelo(this.transform);
                Manager.instance.CurModel = Instantiate(Manager.instance.modelos[indexmodel].modelo, this.transform);
                Manager.instance.CurModel.transform.localScale = Vector3.one;
                Manager.instance.CurModel.transform.localPosition = Vector3.zero;
                Manager.instance.CurModel.transform.localEulerAngles = Vector3.zero;
                print(model.modelo.name);
                if (!Manager.instance.currentUser.objects.Contains(model.modelo.name))
                {
                    Manager.instance.currentUser.objects.Add(model.modelo.name);
                    PersistentUserData.SaveUserData(Manager.instance.currentUser);
                    Manager.instance.ArmaHistorial();
                }
                print("saved");
                modelDownloaded = true;
                return;
            }
            indexmodel++;
        }
    }

    // Update is called once per frame
    void OnDisable()
    {
        print("apaga");
        if (!Manager.instance.modelsCharged)
            return;
        Destroy(Manager.instance.CurModel);
    }
}
