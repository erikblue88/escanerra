﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;
using Vuforia;

public class VideoBH : MonoBehaviour
{
    public VideoPlayer video;
    public ParticleSystem humo;
    public Animator anim;
    public Transform parent;
    public GameObject botonCerrar;

    Vector3 initialPosition;

    private void Awake()
    {
        var url = Application.streamingAssetsPath + "/" + "mujeres FINAL 1 ALTA.mp4";
        video.url = url;
        video.Prepare();
    }

    // Start is called before the first frame update
    void OnEnable()
    {
        initialPosition = transform.localPosition;
        //if (parent.name == "Logo")
        //    anim.Play("Crece Logo");
        //else
            anim.Play("CreceCandidata");
    }

    private void OnDisable()
    {
        transform.localPosition = initialPosition;
        transform.localScale = Vector3.zero;
        //humo.Stop();
        video.Stop();
        if (botonCerrar != null)
            botonCerrar.SetActive(false);
    }

    public void PlayVideo()
    {
        if(botonCerrar != null)
        botonCerrar.SetActive(true);
        //humo.Play();
        video.Play();
    }

    public void Cerrar()
    {
        gameObject.SetActive(false);
        //SceneManager.LoadScene(1);
    }
}
