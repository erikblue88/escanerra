﻿using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class PersistentUserData
{
	public static void SaveUserData(UserData data)
	{
		//Debug.Log("Data to save: " + data.ToString());
		try
		{
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Create(Application.persistentDataPath + "/Escaner.dat");
			bf.Serialize(file, data);
			file.Close();
			Debug.Log("User data saved");
		}
		catch (Exception e)
		{
			Debug.Log("Erroron saving: " + e.Message);
		}
	}

	public static UserData LoadUserData()
	{
		UserData data = new UserData();
		try
		{
			if (File.Exists(Application.persistentDataPath + "/Escaner.dat"))
			{
				BinaryFormatter bf = new BinaryFormatter();
				FileStream file = File.Open(Application.persistentDataPath + "/Escaner.dat", FileMode.Open);
				data = (UserData)bf.Deserialize(file);
				file.Close();
				Debug.Log("User data loaded");
			}
		}
		catch (Exception e)
		{
			Debug.Log("Error on loading: " + e.Message);
		}
		return data;
	}
}
