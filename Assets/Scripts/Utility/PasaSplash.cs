﻿// ************************************************************************ 
// File Name:   PasaSplash.cs 
// Purpose:    	Wait some tume to load main scene
// Project:		ADVArquitecture
// Author:      Erik Marmolejo Ensastegui
// Copyright: 	2020 Inmersys
// ************************************************************************ 

// ************************************************************************ 
// Imports 
// ************************************************************************ 
using UnityEngine;
using UnityEngine.SceneManagement;

public class PasaSplash : MonoBehaviour
{
    public int timeToWait;      //Tiempo para esperar a pasar a la escena principal

    // Awake is called on instantiation of the scene
    void Awake()
    {
        Invoke("CargaMainScene", timeToWait);
    }

    //Manda llamar la escena principal
    void CargaMainScene()
    {
        SceneManager.LoadScene(1);
    }
}
