﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DesarrolloSeleccionado : Singleton<DesarrolloSeleccionado>
{
    public Text title;
    public TextMeshProUGUI descripcion;
    public Transform parentCarrusel;
    public List<Sprite> carrusel;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    
}
