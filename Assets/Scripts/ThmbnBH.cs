﻿using UnityEngine;

public class ThmbnBH : MonoBehaviour
{
    public void OpenWORA(GameObject thmbnl)
    {
        if (Manager.instance.modelWORAClicked)
            return;
        int indexmodel = 0;
        foreach (Modelos model in Manager.instance.modelos)
        {
            if (model.modelo.name == thmbnl.name.Split('(')[0])
            {
                Manager.instance.modelWORAClicked = true;
                Manager.instance.CurModelWORA = Instantiate(Manager.instance.modelos[indexmodel].modelo, Manager.instance.parentWora);
                Manager.instance.CurModelWORA.transform.localScale = Vector3.one;
                Manager.instance.CurModelWORA.transform.localPosition = Vector3.zero;
                Manager.instance.CurModelWORA.transform.localEulerAngles = Vector3.zero;
                Manager.instance.WORAObject.SetActive(true);
                FindObjectOfType<AdminCanvas>().ChangeCanvas(2);
                return;
            }
            indexmodel++;
        }
    }
}
