﻿using Firebase.Storage;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Policy;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using Vuforia;

public class Manager : Singleton<Manager>
{
    public List<Modelos> modelos;
    public List<GameObject> thumbnails;
    public Transform parentTarget;
    public Transform parentThumbnails;
    public Transform parentWora;
    public GameObject CurModel;
    public GameObject CurModelWORA;
    public GameObject WORAObject;
    public GameObject descargando;
    public GameObject mensaje1;
    public GameObject mensaje2;
    public GameObject logoVideo;
    public bool modelsCharged;
    public bool modelWORAClicked;
    public UserData currentUser;

    AssetBundle ab;
    public string URLAB;

    #region private methods
    private void Awake()
    {
        print(Application.persistentDataPath);
        VuforiaRuntimeUtilities.SetAllowedFusionProviders(FusionProviderType.VUFORIA_VISION_ONLY);
        modelos = new List<Modelos>();
        currentUser = PersistentUserData.LoadUserData();
        if (currentUser.objects == null)
        {
            currentUser.objects = new List<string>();
            PersistentUserData.SaveUserData(currentUser);
        }
    }

    private void Start()
    {
        //FirebaseURL();
        //StartCoroutine(CreateList());
        ArmaHistorial();
    }

    public void ArmaHistorial()
    {
        foreach(string cur in currentUser.objects)
            foreach(GameObject thumb in thumbnails)
                if(thumb.name == cur)
                    Instantiate(thumb, parentThumbnails);
    }

    public void FirebaseURL(string nameAssetBundle)
    {
        // Get a reference to the storage service, using the default Firebase App
        FirebaseStorage storage = FirebaseStorage.DefaultInstance;

        // Create a reference from a Google Cloud Storage URI
        StorageReference reference =
          storage.GetReferenceFromUrl("gs://plantillara.appspot.com/AssetBundlesAndroid/" + nameAssetBundle);

        // Fetch the download URL
        reference.GetDownloadUrlAsync().ContinueWith((Task<Uri> task) => {
            if (!task.IsFaulted && !task.IsCanceled)
            {
                Debug.Log("Download URL: " + task.Result);
                URLAB = task.Result.ToString();
                print(URLAB);
                // ... now download the file via WWW or UnityWebRequest.
            }
            else
            {
                Debug.Log(task.Result);
            }
        });
    }

    public void CreateList(string nombreModelo)
    {
        StartCoroutine(createList(nombreModelo));
    }

    void RefreshList(string nombreModelo)
    {
        Modelos m = new Modelos();
        m.modelo = ab.LoadAsset<GameObject>(nombreModelo);
        //m.textura = ab.LoadAsset<Texture>("0");
        modelos.Add(m);
        modelsCharged = true;
        //btnTxt.text = "instancía";
        //instancia.interactable = true;
    }

    void OnDestroy()
    {
        UnloadAssets(false);
    }

    private void OnApplicationQuit()
    {
        UnloadAssets(false);
    }

    void UnloadAssets(bool type)
    {
        if (ab != null)
            ab.Unload(type);
    }

    IEnumerator createList(string nombreModelo)
    {
        //while (string.IsNullOrEmpty(URLAB))
        //{
        //    yield return null;
        //}
        //UnityWebRequest request = UnityWebRequestAssetBundle.GetAssetBundle(URLAB, 0);
        //yield return request.SendWebRequest();
        //ab = DownloadHandlerAssetBundle.GetContent(request);
        //RefreshList();


        while (string.IsNullOrEmpty(URLAB))
        {
            yield return null;
        }

        while (!Caching.ready)
            yield return null;
        descargando.SetActive(true);
        var www = WWW.LoadFromCacheOrDownload(URLAB, 0);
        yield return www;
        if (!string.IsNullOrEmpty(www.error))
        {
            Debug.Log(www.error);
            yield return null;
        }
        UnloadAssets(false);
        ab = www.assetBundle;
        RefreshList(nombreModelo);
        descargando.SetActive(false);
        //mensaje1.SetActive(true);
    }

    IEnumerator muestraModelo(Transform controlContainer)
    {
        while (!modelsCharged)
            yield return null;
        CurModel = Instantiate(modelos[0].modelo, controlContainer);
        CurModel.transform.localScale = Vector3.one;
        CurModel.transform.localPosition = Vector3.zero;
        CurModel.transform.localEulerAngles = Vector3.zero;
    }
    #endregion //private methods

    #region public methods
    public void MuestraModelo(Transform controlContainer)
    {
        StartCoroutine(muestraModelo(controlContainer));
    }
    //public GameObject currentVideo;
    //public void CloseCurrentVideo(GameObject video)
    //{
    //    if (currentVideo != null)
    //        currentVideo.SetActive(false);
    //    currentVideo = video;
    //}
    //public void instanceModel()
    //{
    //    Instantiate(modelos[0].modelo);
    //    instancia.interactable = false;
    //}
    #endregion //public methods
}

[Serializable]
public class Modelos
{
    public GameObject modelo;
    public Texture textura;
}
