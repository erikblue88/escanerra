﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CanvasPartsUp : MonoBehaviour {

	public GameObject adminCanvas;

	//The objects that are inside the Canvas
	public GameObject[] objectsInCanvas;

	//Array of the position of the objects in the canvas
	private Vector3[] objectsPosition;
	//Screen height 
	private float screenHeight;

	// Use this for initialization
	void Start () {
		//make the length of the objectPosition the same as the ObjectsInCanvas
		objectsPosition = new Vector3 [objectsInCanvas.Length];

		//Set the screen height with the rectTransform of the adminCanvas
		screenHeight = adminCanvas.GetComponent<RectTransform>().rect.height;
	}


	public void MoveDownOut(){
		//Save the current position of every objectInCanvas and change it to be out of view
		for(int i = 0; i < objectsInCanvas.Length;i++){
			objectsPosition[i] = objectsInCanvas[i].transform.localPosition;
			Vector3 positionObject = objectsInCanvas[i].transform.localPosition;
			positionObject.y -= screenHeight;
			objectsInCanvas[i].transform.localPosition = positionObject;
		}
	}


	public void MoveUpIn(int index, float timeInSec){
		//start Corouitine
		StartCoroutine(MovePartsIn( index, timeInSec));
	}

	//The coroutine that animates the way it the object enter
	private IEnumerator MovePartsIn(int index, float timeInSec){
		
		//Move every objectInCanvas to the original position
		for(int i = 0; i < objectsInCanvas.Length; i++){
			//Set the posInicial
			Vector3 posInicial = objectsInCanvas[i].transform.localPosition;

			//Move one objec of the Canvas at a time
			float timeTransition = 0;
			while(timeTransition < 1){

				//Add the deltaTime to the timeTransition in a way it takes half of the timeInSec
				timeTransition += Time.deltaTime * ((2*objectsInCanvas.Length)/timeInSec);

				//Move the objectsInCanvas
				objectsInCanvas[i].transform.localPosition = Vector3.Lerp(posInicial,objectsPosition[i],timeTransition);

				yield return null;

			}
			//Position the ObjectInCanvas at the position it should to prevent errors
			objectsInCanvas[i].transform.localPosition = objectsPosition[i];

			yield return null;
		}

		//chage the inTransition variable to false
		AdminCanvas.inTransition = false;
		//Update the active screen
		AdminCanvas.activeScreen = index;


	}
}
