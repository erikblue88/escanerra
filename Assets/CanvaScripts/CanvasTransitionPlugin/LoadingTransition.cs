﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingTransition : MonoBehaviour {

	//This variable will determine how the canvas will enter and exit
	public enum ChangeCanvasForm
	{
		Scale,
		Roll,
	};

	public ChangeCanvasForm changeCanvasEffect;

	//The time it take for the transition to happend
	[ Range (0.1f,4) ]
	public float speedInSec = 1f;
	
	//The time it takes to the loading screen to exit

	[ Range (0.1f,4) ]
	public float timeToExit = 1f;

	//Determine if the loading screen will exit after some time or when some object call the stop function
	public bool waitForExitCall;

	//AdminCanvas
	public GameObject canvasAdministrator;
	
	//Image of the loading sprite
	public GameObject loadingImage;
	
	private RawImage imagenLoading;

	public bool isLoading;

	void Start () {
		//Get the Raw Image component from the loading image
		imagenLoading = loadingImage.GetComponent<RawImage>();
	}
	
	// Update is called once per frame
	void Update () {
		//If is loading sprite will rotate
		if(isLoading){
			loadingImage.transform.Rotate(0, 0, -Time.deltaTime*720);
		}
	}

	//Call the enter transition
	public void StartLoadingMovement(){
		//call a function depending of the changeCanvasType
		switch (changeCanvasEffect)
		{
			case ChangeCanvasForm.Scale:
				canvasAdministrator.GetComponent<Scale>().ScaleLoadingTransitionIn(speedInSec);
				break;
			case ChangeCanvasForm.Roll:
				canvasAdministrator.GetComponent<Roll>().RollLoadingTransitionIn(speedInSec);
				break;
		}
	}

	//Activate the loading animation
	public void ActivateLoading(){
		StartCoroutine(FadeInLoading());
	}

	//Deactivate the loading animation
	public void DeactivateLoading(){
		StartCoroutine(FadeOutLoading());
	}

	//Start the loading animation with a fadeIn of the loading sprite
	public IEnumerator FadeInLoading(){
		isLoading = true;

		//Make the loading image transparent
		float timeAlpha = 0;
		Color colorAlpha = loadingImage.GetComponent<RawImage>().color;
		colorAlpha.a = 0;
		imagenLoading.color = colorAlpha;

		//Activate the loadingImage
		loadingImage.SetActive(true);

		//Make the loadingImage visible
		while(timeAlpha<1){
			timeAlpha += Time.deltaTime * 5;
			colorAlpha.a = timeAlpha;
			imagenLoading.color = colorAlpha;
			yield return null;
		}
		//Check if you will deactivate the loading animation after some time
		if(!waitForExitCall){
			Invoke("DeactivateLoading",timeToExit);
		}

	}


	//Stop the loading animation with a fadeOut of the loading sprite
	public IEnumerator FadeOutLoading(){
		//Make the loading image visible
		float timeAlpha = 1;
		Color colorAlpha = loadingImage.GetComponent<RawImage>().color;
		colorAlpha.a = 1;
		imagenLoading.color = colorAlpha;

		//Make the loadingImage transparent
		while(timeAlpha>0){
			timeAlpha -= Time.deltaTime * 5;
			colorAlpha.a = timeAlpha;
			imagenLoading.color = colorAlpha;
			yield return null;
		}

		//Deactivate the loadingImage and reset its rotation
		loadingImage.SetActive(false);
		isLoading = false;
		loadingImage.transform.Rotate(0, 0,0);

		//call a function depending of the changeCanvasType
		switch (changeCanvasEffect)
		{
			case ChangeCanvasForm.Scale:
				canvasAdministrator.GetComponent<Scale>().ScaleLoadingTransitionOut(speedInSec);
				break;
			case ChangeCanvasForm.Roll:
				canvasAdministrator.GetComponent<Roll>().RollLoadingTransitionOut(speedInSec);
				break;
		}
	}

	//Change the Transition type
	public void SetChangeCanvasType(int transitionType){
		changeCanvasEffect = (ChangeCanvasForm) transitionType;
	}

}
