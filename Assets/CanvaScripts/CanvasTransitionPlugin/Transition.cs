﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transition : MonoBehaviour {
	//Array of all the canvas you will transition to
	public Canvas[] screensCanvas;

	//Index of the canvas you will transition to
	public int index;

	//The canvas that will make the transition
	public Canvas transitionCanvas;

	//Preview Canvas
	public Canvas previousCanvas;
	//Next Canvas
	public Canvas nextCanvas;
	//Menu Canvas
	public Canvas menuCanvas;

}
